+++
date = 2020-10-19
title = "Die ersten Lastenräder sind angekommen"

[extra]
image = "lara.jpg"
+++
Auf der Webseite sind nun die ersten Einträge zu Lastenrädern des Kolara abrufbar. Jedes Rad verfügt über Meta-Informationen, die es Ermöglichen, 
es auf einer Karte anzuzeigen und die Eigenschaften der Räder an verschiednen Stellen auf der Website aufzugreifen. 

Ein weiteres Rad kannst du hinzufügen, in dem du im Ordner `content/lastenrad` auf [Gitlab](https://gitlab.com/kolara/kolara.gitlab.io/) einen neuen Ordner mit dem Namen des Rades erstellst.
In diesen Ordner legst du ein Bild vom Lastenrad ab, das du `image.jpg` nennst und eine Datei namens index.md, die folgendes Template enthält:
```
+++
title = ""
description = ""
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = ""},
  {name = "Standort", entry = ""},
  {name = "Beladung", entry = ""},
  {name = "Email", entry = ""},
  {name = "Details", entry = ""},
]
lat = ""
lon = ""
+++
Hier kann eine kleine Geschichte über das Rad hin oder halt nichts.
```

Alle leeren Stellen zwischen zwei Anführungzeichen, musst du noch mit entsprechendem Inhalt füllen.
