+++
title = "Wie funktioniert's?"
+++

# Idee

Ziel der Initiative „KOLARA Le“ ist Lastenräder als Alternative zu Autos im Alltag nutzbarer und präsenter zu machen.

Wir wollen in Leipzig eine generationsuebergreifende umweltfreundliche Infrastruktur im urbanen Raum schaffen. Mit multifunktionalen Lastenrädern soll Menschen die Möglichkeit gegeben werden, die eigene Mobilität gemeinschaftlich emanzipatorisch gestalten zu koennen.

# Was und Warum ? – ökologischer und sozialer Aspekt

Durch die gemeinschaftlich organisierte Verfügbarmachung im Sinne eines „Bike-Sharing“ und Instandhaltung von menschbetriebenen Spezialtransporträdern soll der Liefer- und Transportverkehr im großstädtischen Raum mitgestaltet und verändert werden. Wichtige Teilaspekte des Kollektives sind:

## Umweltschonend
Der Transport durch Lastenfahrräder ist klimaschonend, geraeuscharm, Ressourcen schonend und schadstoffarm. Dadurch ist eine solche Infrastruktur ein möglicher Beitrag zur postfossilen Mobilität, zur Förderung nachhaltiger Alternativen zu herkömmlichen Lastentransporten und trägt zur Reduzierung des Automobilverkehrs im urbanen Raum bei.

## Förderung von Gemeinschaftsgütern & solidarischen Infrastrukturen 
Durch partizipative Nutzung und Wartung von solidarisch organisierten Lastenfahrrädern und Anhängern als Gemeinschaftsgüter, soll eine emanzipative Mobilität von Mensch und Last gefördert werden. Damit einher geht eine transparente Informationspolitik, um einen möglichst niedrigschwelligen, flexiblen und ueberschaubaren Zugang zu schaffen. Durch welche eine vielseitige und hochfrequentierte Nutzung und selbstbestimmte Mobilität im urbanen Raum gefördert werden soll.

## DIY
Wissensweitergabe/ Transparenz: Verschiedene Fahrradstationen und Selbsthilfewerkstätten sollen als öffentliche/offene Räume genutzt werden um als Lern- und Begegungsstätte wirken zu können.
