
+++
title = "Grüne Welle"
description = "LongJohn"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Long John"},
  {name = "Standort", entry = "Seumestraße, Knautkleeberg/ oder Plagwitz"},
  {name = "Beladung", entry = "100kg"},
  {name = "Email", entry = "gruenewelle@kolara.org"},
  {name = "Details", entry = "Scheibenbremse vorne und hinten, 8-Gang-Nabenschaltung,  Schloss, Dynamolicht und Schutzbleche"},
]
lat = "51.27777986588618"
lon = "12.311468183664353"
+++
Großes und schweres lastenrad, nicht für nervöse Fahrer*innen! dafür für große Sachen geeignet. Verstellbare Sattelhöhe (bitte 13mm Schlüssel mitbringen und wieder zurücksetzen).
