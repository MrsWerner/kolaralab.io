+++
title = "rakete"
description = "long-john"
template = "zeug.html"

[extra]
thumbnail = "rakete.jpg"
properties = [
  {name = "Bautyp", entry = "Longjohn"},
  {name = "Standort", entry = "Röntgenstraße, Altlindenau"},
  {name = "Beladung", entry = "100 kg, zwei Eurokisten"},
  {name = "Email", entry = "rakete@kolara.org"},
  {name = "Details", entry = "Röntgenblick, 8-fach Raketenantrieb in 2 Zündstufen"},
]
lat = "51.3368"
lon = "12.32308"
+++