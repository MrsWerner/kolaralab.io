+++
title = "lara"
description = "zweirad"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Long John"},
  {name = "Standort", entry = "Josephstraße, Lindenau"},
  {name = "Beladung", entry = "100kg"},
  {name = "Email", entry = "lara@kolara.org"},
  {name = "Details", entry = "Nabenschaltung"},
]
lat = "51.3355"
lon = "12.37457"
+++
Lara gibt es schon sehr lange in Kolara. Sie wurde irgendwann vor 2013 durch das Kollektiv gekauft.
