+++
title = "siebfreak"
description = "dreirad"
template = "zeug.html"

[extra]
thumbnail = "siebfreak.jpg"
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Josephstraße"},
  {name = "Beladung", entry = "200kg"},
  {name = "Email", entry = "siebfreak@kolara.org"},
  {name = "Details", entry = "7-Gang Nabenschaltung mit Rücktrittbremse, Nabendynamo"},
]
lat = "51.3345"
lon = "12.37437"
+++


**Siebfreak** ist eines der ältesten Lastenräder im Kolara.
