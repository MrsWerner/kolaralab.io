+++
title = "sattelschlepper"
description = "long-tail"
template = "zeug.html"

[extra]
thumbnail = "sattelschlepper.jpg"
properties = [
  {name = "Bautyp", entry = "LongTail"},
  {name = "Standort", entry = "Schirmerstraße, Anger-Crottendorf"},
  {name = "Beladung", entry = "80 kg, drei Bierkisten"},
  {name = "Email", entry = "sattelschlepper@kolara.org"},
  {name = "Details", entry = "Komplettrecycling, langer Rahmen, nach hinten erweitert, lenkt sich wie ein nicht-Lastenrad"},
]
lat = "51.333802097858275"
lon = "12.41028072029713"
+++
