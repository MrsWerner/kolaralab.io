+++
title = "Hirschkolbensumach"
description = "LongJohn"
template = "zeug.html"

[extra]
properties = [
  {name = "Bautyp", entry = "Long John Monster"},
  {name = "Standort", entry = "Dorfplatz Stünz, Sellerhausen"},
  {name = "Beladung", entry = "100kg"},
  {name = "Email", entry = "hks@kolara.org"},
  {name = "Details", entry = "Kettenschaltung"},
]
lat = "51.33865778948033"
lon = "12.431975795248585"
+++
Unterbodenbeleuchtung; Auch Transport langer Holzstücke durch speziellen Schutz des vorderen Steuerlagers möglich. Anhängerkupplung vorhanden – zusätzlicher Anhänger bei Bedarf.