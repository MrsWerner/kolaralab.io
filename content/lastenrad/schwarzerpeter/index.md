+++
title = "Schwarzer Peter"
description = "dreirad"
template = "zeug.html"

[extra]
thumbnail = "schwarzerpeter.jpg"
properties = [
  {name = "Bautyp", entry = "Dreirad"},
  {name = "Standort", entry = "Eisenbahnstraße, Radsfatz"},
  {name = "Beladung", entry = "80kg"},
  {name = "Email", entry = "schwarzerpeter@kolara.org"},
  {name = "Details", entry = "Nabenschaltung, Licht, Laderampe ausklappbar"},
]
lat = "51.345187046628496"
lon = "12.412623022486837"
+++

Der große Hebel am Lenker bremst auch. Es muss stets versucht werden, das Rad einem anderen Mitspieler unterzujubeln.