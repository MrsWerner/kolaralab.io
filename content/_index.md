+++
title = "Dein Lastenradkollektiv in Leipzig"
description = "KOLARA ist ein Zusammenschluss von Menschen, die Lastenräder nutzen, verleihen und bauen."

[extra]
menu = [
    { text = "Ausleihen", link = "/lastenrad" },
    { text = "Mitmachen", link = "/about" },
]
more_text = "Mehr dazu ⇩"
content_title = "Umsetzung"
+++

# Organisation

Die Organisation des KOLARA (Beschaffung von Lastenfahrrädern, Bau, Instandhaltung/Wartung und Verleihstruktur) wird von Beteiligten und Nutzerinnen selbstorganisiert. Es wird angestrebt, dass sich möglichst viele verschiedene Menschen mit der Infrastruktur auseinander setzen koennen. Sicherlich werden sich erst eine wenige Menschen mit den verschiedenen Aufgaben beschaeftigen. Jedoch ist die Bestrebung die individuelle Beteiligung und Befähigung in einem kontinuierlichen Prozess kollegial weiter zu entwickeln und zu erweitern.

# Partizipation

Grundsätzlich ist jedem offen inwieweit Mensch sich an der Struktur beteiligt.  Das Verleihen der Räder geschieht dabei immer zwischen Pat_innen und Nutzenden. Alle weiteren Menschen kümmern sich in Prinzip darum, diesen Austausch zu ermöglichen.

## Paten
Ziel der  Patenschaften ist die dezentrale Organisation des Verteilsystems. Jedes Lastenfahrrad und/oder Anhaenger bekommen eins bis zwei zugehoerige Pat_innen, welche als Ansprechpartner_innen fungieren. Räder werden von Radpat_innen gepflegt und verliehen.

## Nutzende
Alle diejenigen, die ein Rad ausleihen möchten wenden sich an die entsprechenden Pat_innen, diese haben einen Überblick über die Verfügbarkeit und die Besonderheiten der Räder und ermöglichen den Zugang. Um zum ersten Mal mit Pat_innen in Kontakt zu treten, dienen in der Regel die Mailadressen der Lastenräder. Das Kommunikationsmedium spielt für die Ausleihe aber prinzipiell keine Rolle. Wichtig ist einzig die Absprache mit der jeweiligen Pat_in.

## Andere Unterstützende
Wir freuen uns über alle, die die Lust haben dabei zu helfen, neue Lastenräder zu bauen und die Vorhandenen zu warten. Auch bei der medialen Betreuung des Projektes könnt ihr KOLARA unterstützen und schließlich freuen wir und über Alle, die die Idee weitertragen und weiterentwickeln.

Die Finanzierung des Eigenbaus bzw. Ankaufs dieser Lastenräder ist von Spenden und Förderungen abhängig. Dies soll einerseits über Solipartys geschehen, andererseits werden wir versuchen, Förderungen für die Lastenfahrräder zu bekommen. Ein zweiter Punkt, der in weiterer Folge Geld braucht, ist ein Reperaturfond, aus dem wir Ersatzteile wie Schläuche, Bremsbacken, Ketten, Öl,… kaufen wollen, die für den Betrieb der Räder notwendig sind.

# Lastenräder

## Teilen
Generell sind alle Menschen, die bereits ein Lastenrad besitzen, eingeladen sich im Kollektiv zu organisieren und Pat_innen zu werden und es anderen Nutzenden zur Verfügung zu stellen.
## Bauen
Sehr oft entstehen die Räder des Kollektivs aber in Lastenradbau-Workshops.
Dazu werden wir von mehreren selbst verwalteten Fahrrad-Selbsthilfe-Werkstätten unterstützt. 
## Wartung
Die Wartung wird in der Regel durch die Pat_innen organisiert. Dazu können sie auf die Selbsthilfewerkstätten sowie auf finanzielle Unterstützung seitens des KOLARA zurückgreifen. Außerdem können Reperaturen und Wartungen mit den Nutzenden abgesprochen werden, z.B falls bei Ausleihen etwas kaputt gegangen ist. 
